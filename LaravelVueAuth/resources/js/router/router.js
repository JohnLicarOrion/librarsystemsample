import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

import Login from "./../components/views/auth/Login.vue";
import Register from "./../components/views/auth/Register.vue";
import IndexMain from "./../components/views/main/indexMain.vue";
import Dashboard from "./../components/views/main/Dashboard.vue";
import Accounts from "./../components/views/main/Account.vue";

const routes = [
    {
        path: "/",
        name: "Main",
        component: IndexMain,

        meta: { requiresAuth: true },
        children: [
            {
                path: "/dashboard",
                name: "Dashboard",

                meta: { requiresAuth: true },
                components: {
                    default: IndexMain,
                    MainView: Dashboard
                }
            },
            {
                path: "/accounts",
                name: "Accounts",
                meta: { requiresAuth: true },
                components: {
                    default: IndexMain,
                    MainView: Accounts
                }
            }
        ]
    },
    {
        path: "/login",
        name: "Login",
        component: Login,
        meta: { guest: true }
    },
    {
        path: "/register",
        name: "Register",
        component: Register,
        meta: { guest: true }
    }
];

const router = new VueRouter({
    mode: "history",
    linkActiveClass: "active",
    routes
});

function loggedIn() {
    return localStorage.getItem("token");
}

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (!loggedIn()) {
            next({
                path: "/login",
                query: { redirect: to.fullPath }
            });
        } else {
            next();
        }
    } else if (to.matched.some(record => record.meta.guest)) {
        if (loggedIn()) {
            next({
                path: "/dashboard",
                query: { redirect: to.fullPath }
            });
        } else {
            next();
        }
    } else {
        next(); // make sure to always call next()!
    }
});

export default router;
